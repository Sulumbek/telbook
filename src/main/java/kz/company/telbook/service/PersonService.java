package kz.company.telbook.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import kz.company.telbook.entity.PersonEntity;
import kz.company.telbook.model.PersonDto;
import kz.company.telbook.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PersonService {

  private final PersonRepository personRepository;

  private final ModelMapper modelMapper;

  private final JdbcTemplate jdbcTemplate;

  public PersonDto createPerson(PersonDto dto) {
    PersonEntity entity = personRepository
        .save(modelMapper.map(dto, PersonEntity.class));

    return modelMapper.map(entity, PersonDto.class);
  }

  public void deletePerson(Integer id) {
    personRepository.deleteById(id);
  }

  public PersonDto getPersonById(@NotNull Integer id) {
    Optional<PersonEntity> optional = personRepository.findById(id);
    if (!optional.isPresent()) {
      log.error("Person not found by id {}", id);
    }
    return optional.map(item -> modelMapper.map(item, PersonDto.class))
        .orElse(null);
  }

  public List<PersonDto> getPersons() {
    SqlRowSet row = jdbcTemplate.queryForRowSet("select * from telbook.person");
    List<PersonDto> dtos = new ArrayList<>();
    while (row.next()) {
      String name = row.getString("name");
      String surname = row.getString("surname");
      int age = row.getInt("age");
      PersonDto dto = new PersonDto(name, surname, age);
      dtos.add(dto);
    }

    List<Map<String, Object>> personList = personRepository.getAllContactAndPerson();

    for (Map<String, Object> map : personList) {
      String name = (String) map.get("name");
      String surname = (String) map.get("surname");
      Integer age = (Integer) map.get("age");
      PersonDto dto = new PersonDto(name, surname, age);
      dtos.add(dto);
    }


    List<PersonEntity> entities = personRepository.getPersonsEntities();

    List<PersonEntity> e = personRepository.findAll();

    return entities.stream()
        .map(item -> modelMapper.map(item, PersonDto.class))
        .collect(Collectors.toList());
  }

  public PersonDto updatePerson(PersonDto dto) {
    boolean result = personRepository.existsById(dto.getId());

    if (!result) {
      log.warn("Error is id not found: {}", dto.getId());
      throw new RuntimeException("ERROR");
    }
    return this.createPerson(dto);
  }

}
