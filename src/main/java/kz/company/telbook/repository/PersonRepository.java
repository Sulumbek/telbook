package kz.company.telbook.repository;


import java.util.List;
import java.util.Map;
import kz.company.telbook.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Integer> {

  List<PersonEntity> findAllByNameLike(String name);

  @Query(value = "select * from telbook.person", nativeQuery = true)
  List<Map<String, Object>> getAllContactAndPerson();

  @Query(value = "select * from telbook.person where name = ?1 and surname = ?2", nativeQuery = true)
  List<PersonEntity> getPersonsEntities(String name, String surname);

  @Query(value = "select * from telbook.person where name = :name and surname = :surname", nativeQuery = true)
  List<PersonEntity> getPersonsEntities2(@Param("name") String name, @Param("surname") String surname);


}
