package kz.company.telbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Where;

@Data
@Entity
@Table(name = "car", schema = "telbook")
@Where(clause = "active = 'ACTIVE'")
public class CarEntity {

  @Id
  @Column(name = "id")
  @GeneratedValue(
      strategy = GenerationType.SEQUENCE,
      generator = "car_seq")
  @SequenceGenerator(
      name = "car_seq",
      sequenceName = "car_id_seq", schema = "telbook", allocationSize = 1)
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "model")
  private String model;

}
