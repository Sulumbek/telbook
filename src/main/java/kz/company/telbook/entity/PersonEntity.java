package kz.company.telbook.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "person", schema = "telbook")
public class PersonEntity {

  @Id
  @Column(name = "id")
  @GeneratedValue(
      strategy = GenerationType.SEQUENCE,
      generator = "person_seq")
  @SequenceGenerator(
      name = "person_seq",
      sequenceName = "person_id_seq", schema = "telbook", allocationSize = 1)
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "surname")
  private String surname;

  @Column(name = "age")
  private Integer age;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "person_cars",
      joinColumns = @JoinColumn(name="person_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "car_id", referencedColumnName = "id")
  )
  @OrderBy("model asc")
  private List<CarEntity> cars;

  @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinColumn(name = "person_id")
  List<ContactEntity> contacts;



}
