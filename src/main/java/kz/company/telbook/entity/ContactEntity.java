package kz.company.telbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Where;

@Data
@Entity
@Table(name = "contact", schema = "telbook")
public class ContactEntity {

  @Id
  @Column(name = "id")
  @GeneratedValue(
      strategy = GenerationType.SEQUENCE,
      generator = "contact_seq")
  @SequenceGenerator(
      name = "contact_seq",
      sequenceName = "contact_id_seq", schema = "telbook", allocationSize = 1)
  private Integer id;

  @Column(name = "type")
  private String type;

  @Column(name = "value")
  private String value;

}
