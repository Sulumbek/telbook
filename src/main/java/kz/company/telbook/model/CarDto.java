package kz.company.telbook.model;

import lombok.Data;

@Data
public class CarDto {

  private Integer id;

  private String name;

  private String model;

//  @Column(name = "person_id")
//  private Integer personId;

}
