package kz.company.telbook.model;

import java.util.List;
import kz.company.telbook.entity.CarEntity;
import kz.company.telbook.entity.ContactEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class PersonDto {

  private Integer id;

  private String name;

  private String surname;

  private Integer age;

  private List<CarDto> cars;

  private List<ContactDto> contacts;

  public PersonDto(String name, String surname, int age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
  }
}


// {id: null, name: "Salamo", surname: "Bisultanov", age: "33" } for create

// {id: 22, name: "Salamo", surname: "Bisultanov", age: "33" } for update
