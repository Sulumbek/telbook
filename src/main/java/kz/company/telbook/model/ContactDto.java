package kz.company.telbook.model;

import lombok.Data;

@Data
public class ContactDto {

  private Integer id;

  private String type;

  private String value;

}
