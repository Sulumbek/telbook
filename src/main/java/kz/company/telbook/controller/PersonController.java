package kz.company.telbook.controller;

import java.util.List;
import kz.company.telbook.model.PersonDto;
import kz.company.telbook.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/person")
public class PersonController {

  private final PersonService personService;

  @GetMapping(path = "/id")
  private ResponseEntity<PersonDto> getPersonById(@RequestParam(name = "id") Integer id) {
    return ResponseEntity.ok(personService.getPersonById(id));
  }

  @GetMapping(path = "/all")
  private ResponseEntity<List<PersonDto>> getPersons() {
    return ResponseEntity.ok(personService.getPersons());
  }

  @PostMapping(path = "/create")
  private ResponseEntity<PersonDto> createPerson(@RequestBody PersonDto dto) {
    return ResponseEntity.ok(personService.createPerson(dto));
  }

  @PutMapping(path = "/update")
  private ResponseEntity<PersonDto> updatePerson(@RequestBody PersonDto dto) {
    return ResponseEntity.ok(personService.updatePerson(dto));
  }

  @DeleteMapping(path = "/delete")
  private ResponseEntity<?> deletePerson(@RequestParam(name = "id") Integer id) {
    personService.deletePerson(id);
    return ResponseEntity.ok().build();
  }

}
