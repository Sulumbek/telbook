
CREATE TABLE telbook.car
(
    id        SERIAL PRIMARY KEY,
    model     varchar,
    name     varchar,
    person_id INTEGER,
    CONSTRAINT fk_person
        FOREIGN KEY (person_id)
            REFERENCES telbook.person (id)
            ON DELETE CASCADE
);
