CREATE TABLE telbook.person
(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(255),
    surname VARCHAR(255),
    age     INTEGER
);

CREATE TYPE contact_enum AS ENUM ('EMAIL', 'PHONE', 'FACEBOOK');

CREATE TABLE telbook.contact
(
    id        SERIAL PRIMARY KEY,
    type     telbook.contact_enum,
    value     VARCHAR(255),
    person_id INTEGER,
    CONSTRAINT fk_person
        FOREIGN KEY (person_id)
            REFERENCES telbook.person (id)
            ON DELETE CASCADE
);
